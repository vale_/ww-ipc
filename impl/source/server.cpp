/*
 * Copyright (c) 2022 Valentin B.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ipc/server.hpp"

#include <windows.h>

#include "ipc/shared_memory.hpp"

namespace ww {

    Server::Server(char *server_addr)
        : m_server_addr(server_addr),
          m_mapped_file(mem::CreateMemoryFile(m_server_addr, sizeof(RingBuffer))),
          m_buffer(RingBufferGuard(mem::MapView(m_mapped_file, sizeof(RingBuffer)))),
          m_data_signal(::CreateEventA(nullptr, FALSE, FALSE, "WalkTheWiz_DataEvent")),
          m_block_signal(::CreateEventA(nullptr, FALSE, FALSE, "WalkTheWiz_BlockEvent")) {}

    Server::~Server() {
        m_buffer.Destroy();
        mem::UnmapView(m_buffer);
        ::CloseHandle(m_mapped_file);
        ::CloseHandle(m_data_signal);
        ::CloseHandle(m_block_signal);
    }

    RingBuffer::Block *Server::AcquireBlock(unsigned long timeout) {
        while (true) {
            /* Get the current block the cursor points to. */
            long block_idx = (*m_buffer).GetReadStartCursor();
            auto *block    = (*m_buffer).GetBlock(block_idx);

            /* Check if we have written blocks left or wait until one becomes populated. */
            if (block->GetNext() == (*m_buffer).GetWriteEndCursor()) {
                /* No space available, wait until we have it. */
                if (::WaitForSingleObject(m_data_signal, timeout) == WAIT_OBJECT_0) {
                    continue;
                }

                /* We timed out. */
                return nullptr;
            }

            /* Try to set the new block cursor and return the block on success. */
            if ((*m_buffer).AtomicallyUpdateReadStartCursor(block->GetNext(), block_idx) == block_idx) {
                return block;
            }
        }
    }

    void Server::SustainBlock(RingBuffer::Block *block) {
        block->SetReadFlag();

        while (true) {
            /* Get the current block the cursor points to. */
            long block_idx = (*m_buffer).GetReadEndCursor();
            block          = (*m_buffer).GetBlock(block_idx);

            /* Attempt to clear the read flag and return on success. */
            if (::InterlockedCompareExchange(&block->m_read_flag, 0, 1) != 1) {
                return;
            }

            /* Update the read cursor to the next block. */
            (*m_buffer).AtomicallyUpdateReadEndCursor(block->GetNext(), block_idx);

            /* Signal availability of more data for other waiting threads. */
            if (block->m_previous_idx == (*m_buffer).GetWriteStartCursor()) {
                block->SetResponded();
                ::SetEvent(m_block_signal);
            }
        }
    }

    void Server::ProcessNext(void *buffer, std::size_t size, unsigned long timeout) {
        /* Acquire a block. */
        auto *block = AcquireBlock(timeout);
        if (!block) {
            return;
        }

        /* Read data out of the block. */
        auto amount = block->ReadData(buffer, size);

        /* Process the message. */
        Handler(block, buffer, amount);

        /* Sustain the block. */
        SustainBlock(block);
    }

}
