/*
 * Copyright (c) 2022 Valentin B.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ipc/shared_memory.hpp"

#include <windows.h>

namespace ww::mem {

    constexpr inline DWORD AccessMask = PAGE_READWRITE | SEC_COMMIT;

    void *CreateMemoryFile(const char *name, std::size_t size) {
        /* Abort immediately if we are not given a valid name for the file. */
        if (name == nullptr || name[0] == '\0') {
            return nullptr;
        }

        /* Try to create the file mapping. */
        HANDLE handle = ::CreateFileMapping(INVALID_HANDLE_VALUE, nullptr, AccessMask, 0, size, name);
        if (::GetLastError() == ERROR_ALREADY_EXISTS) {
            ::CloseHandle(handle);
            handle = INVALID_HANDLE_VALUE;
        }

        /* Make sure we didn't encounter any errors along the way. */
        if (handle == nullptr || handle == INVALID_HANDLE_VALUE) {
            return nullptr;
        }

        return handle;
    }

    void *OpenExistingMemoryFile(const char *name) {
        /* Abort immediately if we are not given a valid name for the file. */
        if (name == nullptr || name[0] == '\0') {
            return nullptr;
        }

        /* Try to open an existing file mapping. */
        HANDLE handle = ::OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, name);
        if (handle == nullptr || handle == INVALID_HANDLE_VALUE) {
            return nullptr;
        }

        return handle;
    }

    void *MapView(void *file_handle, std::size_t size) {
        /* Abort immediately if we are not given a valid handle for the file. */
        if (file_handle == nullptr || file_handle == INVALID_HANDLE_VALUE) {
            return nullptr;
        }

        HANDLE handle = ::MapViewOfFile(file_handle, FILE_MAP_ALL_ACCESS, 0, 0, size);
        if (handle == nullptr) {
            return nullptr;
        }

        return handle;
    }

    bool UnmapView(void *file_handle) {
        /* Abort immediately if we are not given a valid handle for this file. */
        if (file_handle == nullptr || file_handle == INVALID_HANDLE_VALUE) {
            return FALSE;
        }

        return ::UnmapViewOfFile(file_handle) == TRUE;
    }

}
