/*
 * Copyright (c) 2022 Valentin B.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ipc/ring_buffer.hpp"

#include <windows.h>

#include <algorithm>
#include <cstring>

namespace ww {

    std::size_t RingBuffer::Block::ReadData(void *buffer, std::size_t size) {
        auto amount = std::min(static_cast<std::size_t>(m_size), size);
        std::memcpy(buffer, m_data, amount);

        return amount;
    }

    std::size_t RingBuffer::Block::WriteData(const void *buffer, std::size_t size) {
        auto amount = std::min(size, BlockSize);
        std::memcpy(m_data, buffer, amount);
        m_size = size;

        return amount;
    }

    RingBuffer::RingBuffer() {
        int next = 1;

        /* Start the ring. */
        m_blocks[0].m_next_idx     = 1;
        m_blocks[0].m_previous_idx = BlockCount - 1;

        /* Link all linear blocks together. */
        for (/* ... */; next < BlockCount - 1; ++next) {
            m_blocks[next].m_next_idx     = next + 1;
            m_blocks[next].m_previous_idx = next - 1;
        }

        /* Close the ring. */
        m_blocks[next].m_next_idx     = 0;
        m_blocks[next].m_previous_idx = BlockCount - 2;

        /* Initialize all ring cursors. */
        m_read_end_cursor    = 0;
        m_read_start_cursor  = 0;
        m_write_end_cursor   = 1;
        m_write_start_cursor = 1;
    }

    long RingBuffer::AtomicallyUpdateReadStartCursor(long new_cursor, long block_idx) {
        return ::InterlockedCompareExchange(&m_read_start_cursor, new_cursor, block_idx);
    }

    long RingBuffer::AtomicallyUpdateReadEndCursor(long new_cursor, long block_idx) {
        return ::InterlockedCompareExchange(&m_read_end_cursor, new_cursor, block_idx);
    }

    long RingBuffer::AtomicallyUpdateWriteStartCursor(long new_cursor, long block_idx) {
        return ::InterlockedCompareExchange(&m_write_start_cursor, new_cursor, block_idx);
    }

    long RingBuffer::AtomicallyUpdateWriteEndCursor(long new_cursor, long block_idx) {
        return ::InterlockedCompareExchange(&m_write_end_cursor, new_cursor, block_idx);
    }

}
