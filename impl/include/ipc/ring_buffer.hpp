/*
 * Copyright (c) 2022 Valentin B.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <memory>
#include <type_traits>

#include "ipc/types.hpp"
#include "ipc/utils/literals.hpp"

namespace ww {

    class Client;
    class Server;

    class RingBuffer {
    public:
        /* A block in the ring buffer. */
        class Block {
        public:
            friend class RingBuffer;
            friend class Client;
            friend class Server;

            static constexpr std::size_t BlockSize = 1_KB;

        private:
            /* Previous and next blocks in the ring. */
            long m_previous_idx;
            long m_next_idx;

            /* Flags to signal access to a block. */
            volatile long m_read_flag;
            volatile long m_write_flag;

            /* Reserved data block along with size of contents. */
            unsigned long m_size;
            volatile bool m_responded;
            alignas(BITSIZEOF(u8)) u8 m_data[BlockSize];

        public:
            Block() = default;

            /* Getters for linked block indices. */
            [[nodiscard]] constexpr inline long GetPrevious() const { return m_previous_idx; }
            [[nodiscard]] constexpr inline long GetNext()     const { return m_next_idx;     }

            /* Indicates whether this block has already been responded to by the server. */
            [[nodiscard]] constexpr inline bool IsResponded() const { return m_responded; }

        private:
            /* Setters for the responded status before and after server processing. */
            constexpr inline void SetResponded()   { m_responded = true;  }
            constexpr inline void ClearResponded() { m_responded = false; }

            /* Setters for read event signal flags. */
            constexpr inline void SetReadFlag()   { m_read_flag = 1; }

            /* Setters for write event signal flags. */
            constexpr inline void SetWriteFlag()   { m_write_flag = 1; }

        public:
            std::size_t ReadData(void *buffer, std::size_t size);

            std::size_t WriteData(const void *buffer, std::size_t size);

            template <typename Value, std::enable_if_t<std::is_trivially_copyable_v<Value>, bool> = true>
            std::size_t WriteData(const Value value) {
                return WriteData(&value, sizeof(Value));
            }
        };

        static constexpr std::size_t BlockCount = 128;

    private:
        /* Cache of all blocks. Keep this the first member for size optimizations. */
        Block m_blocks[BlockCount];

        /* Read and write cursors into the block list. */
        volatile long m_read_end_cursor;
        volatile long m_read_start_cursor;
        volatile long m_write_end_cursor;
        volatile long m_write_start_cursor;

    public:
        /* Default constructor to initialize the ring. */
        RingBuffer();

        /* Getters for all the cursors. */
        [[nodiscard]] constexpr inline long GetReadStartCursor()  const { return m_read_start_cursor;  }
        [[nodiscard]] constexpr inline long GetReadEndCursor()    const { return m_read_end_cursor;    }
        [[nodiscard]] constexpr inline long GetWriteStartCursor() const { return m_write_start_cursor; }
        [[nodiscard]] constexpr inline long GetWriteEndCursor()   const { return m_write_end_cursor;   }

        /* Getters for specific blocks in the ring based on supplied cursor. */
        [[nodiscard]] constexpr inline Block *GetBlock(long cursor) {
            return &m_blocks[cursor];
        }

        /* Atomically update the cursors to point to the next block in the ring. */
        long AtomicallyUpdateReadStartCursor(long new_cursor, long block_idx);
        long AtomicallyUpdateReadEndCursor(long new_cursor, long block_idx);
        long AtomicallyUpdateWriteStartCursor(long new_cursor, long block_idx);
        long AtomicallyUpdateWriteEndCursor(long new_cursor, long block_idx);
    };

    class RingBufferGuard {
    public:
        WW_DISALLOW_COPY(RingBufferGuard);

    private:
        RingBuffer *m_ring = nullptr;

    public:
        template <typename... Args>
        explicit inline RingBufferGuard(void *ring, Args &&...args) {
            if (ring != nullptr) {
                auto *ring_buffer = reinterpret_cast<RingBuffer *>(ring);

                std::construct_at(ring_buffer, std::forward<Args>(args)...);
                m_ring = std::launder(ring_buffer);
            }
        }

        ~RingBufferGuard() { Destroy(); }

        inline void Destroy() {
            if (m_ring != nullptr) {
                std::destroy_at(m_ring);
                m_ring = nullptr;
            }
        }

        static inline RingBuffer *GetPointer(void *ring) {
            return std::launder(reinterpret_cast<RingBuffer *>(ring));
        }

        inline operator RingBuffer *() { return m_ring; }
    };

}
