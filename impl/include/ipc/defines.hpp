/*
 * Copyright (c) 2022 Valentin B.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <limits.h>

/* Declares a class to be non-copyable by deleting the copy constructor. */
#define WW_DISALLOW_COPY(_TYPE) _TYPE(const _TYPE &) = delete

/* Gets the size of a given type in bits. */
#define BITSIZEOF(x) (sizeof(x) * CHAR_BIT)
