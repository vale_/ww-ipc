/*
 * Copyright (c) 2022 Valentin B.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <concepts>

#include "ipc/defines.hpp"
#include "ipc/ring_buffer.hpp"

namespace ww {

    class Client {
    public:
        WW_DISALLOW_COPY(Client);

        static constexpr unsigned long InfiniteTimeout = 0xFFFFFFFF;

    private:
        /* Address of the server. */
        char *m_server_addr;

        /* Handle to the memory-mapped file and its contents. */
        void *m_mapped_file;
        RingBuffer *m_buffer;

        /* Events to signal when blocks were written and when free ones become available. */
        void *m_data_signal;
        void *m_block_signal;

    public:
        Client() = delete;

        explicit Client(char *server_addr);

        ~Client();

        /* Waits for a block to become available. */
        bool WaitForBlock(unsigned long timeout = InfiniteTimeout);

        /* Acquires the next free block, potentially blocking until one is sustained. */
        RingBuffer::Block *AcquireBlock(unsigned long timeout = InfiniteTimeout);

        /* Submits a pre-prepared block to the server for processing. */
        void SubmitBlock(RingBuffer::Block *block);

        /* Shortcut for submitting any data to the server without caring about the response. */
        std::size_t Write(const void *buffer, std::size_t size, unsigned long timeout = InfiniteTimeout);
    };

}
