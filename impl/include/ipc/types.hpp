/*
 * Copyright (c) 2022 Valentin B.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <cstdint>
#include <limits>

using u8  = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;

using i8  = std::int8_t;
using i16 = std::int16_t;
using i32 = std::int32_t;
using i64 = std::int64_t;

using f32 = float;
using f64 = double;

static_assert(std::numeric_limits<f32>::is_iec559);
static_assert(std::numeric_limits<f64>::is_iec559);

/* Creates a bit mask from a bit number. */
#ifndef BIT
#define BIT(n) (1u << (n))
#endif

/* Creates a long bitmask from a bit number. */
#ifndef BITL
#define BITL(n) (1ul << (n))
#endif

/* Creates a bitmask spanning the `n` least significant bits. */
#ifndef LSB
#define LSB(n) (BIT(n) - 1u)
#endif

/* Creates a long bitmask spanning the `n` least significant bits. */
#ifndef LSBL
#define LSBL(n) (BITL(n) - 1ul)
#endif

/* Creates a bitmask from start (inclusive) to end (exclusive) for range extraction. */
#ifndef MASK
#define MASK(start, end) (LSB(end) & ~LSB(start))
#endif

/* Creates a long bitmask from start (inclusive) to end (exclusive) for range extraction. */
#ifndef MASKL
#define MASKL(start, end) (LSBL(end) & ~LSBL(start))
#endif
