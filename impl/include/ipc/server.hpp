/*
 * Copyright (c) 2022 Valentin B.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include "ipc/defines.hpp"
#include "ipc/ring_buffer.hpp"

namespace ww {

    class Server {
    public:
        WW_DISALLOW_COPY(Server);

        static constexpr unsigned long InfiniteTimeout = 0xFFFFFFFF;

    private:
        /* Address of the server. */
        char *m_server_addr;

        /* Handle to the memory-mapped file and its contents. */
        void *m_mapped_file;
        RingBufferGuard m_buffer;

        /* Events to signal when blocks were written and when free ones become available. */
        void *m_data_signal;
        void *m_block_signal;

    private:
        /* Handler callback dispatched for every received message. */
        virtual void Handler(RingBuffer::Block *block, const void *buffer, std::size_t size) = 0;

    public:
        Server() = delete;

        explicit Server(char *server_addr);

        ~Server();

        /* Acquires the next occupied block, potentially waiting until one is submitted. */
        RingBuffer::Block *AcquireBlock(unsigned long timeout = InfiniteTimeout);

        /* Sustains the processed block for the client to read its response. */
        void SustainBlock(RingBuffer::Block *block);

        /* Reads a message from the next block and processes it. */
        void ProcessNext(void *buffer, std::size_t size, unsigned long timeout = InfiniteTimeout);
    };

}
